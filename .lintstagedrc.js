module.exports = {
  '*.{js,json,css,scss,md,ts,html,graphql,yml}': [
    'yarn prettier --write',
    'git add',
  ],
};
