describe('app', () => {
  describe('no action', () => {
    it('should fail if no action is provided', async () => {
      expect.assertions(1);

      try {
        await runCommand('node app.js');
      } catch ({ code }) {
        expect(code > 0).toBe(true);
      }
    });
  });

  describe('filter', () => {
    describe('ry', () => {
      it('should only return the animals whose name ends with ry', async () => {
        const expected = require('./fixtures/filter/expected/filter_by_ry');

        const { code, data: result } = await runCommand(
          'node app.js --filter=ry'
        );

        expect(code).toBe(0);
        expect(JSON.parse(result)).toMatchObject(expected);
      });

      it('should print nothing if the resulting array is empty', async () => {
        const { code, data: result } = await runCommand(
          'node app.js --filter=justin'
        );

        expect(code).toBe(0);
        expect(result).toBe('');
      });
    });
  });

  describe('count', () => {
    it('should return the data with the number of children appended to the name', async () => {
      const expected = require('./fixtures/count/expected/count');

      const { code, data: result } = await runCommand('node app.js --count');

      expect(code).toBe(0);
      expect(JSON.parse(result)).toMatchObject(expected);
    });
  });
});
