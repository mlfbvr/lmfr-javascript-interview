function print(json) {
  console.log(JSON.stringify(json, undefined, 2));
}

try {
  const result = require('./src/cli.js')(process.argv.slice(2));
  if (result.length > 0) {
    print(result);
  }
  process.exit(0);
} catch (err) {
  console.error(err);
  process.exit(1);
}
