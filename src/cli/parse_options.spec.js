const parseOptions = require('./parse_options');

describe('cli/parse_options', () => {
  describe('parseOptions', () => {
    it('should parse the cli unattached options', () => {
      const args = ['justin'];

      expect(parseOptions(args)).toMatchObject({ _: ['justin'] });
    });

    it('should parse the cli valued options', () => {
      const args = ['--filter=justin'];

      expect(parseOptions(args)).toMatchObject({ filter: 'justin' });
    });

    it('should parse the cli unvalued options', () => {
      const args = ['--count'];

      expect(parseOptions(args)).toMatchObject({ count: true });
    });

    it('should parse the cli args', () => {
      const args = ['--count', '--filter=justin', 'justin'];

      expect(parseOptions(args)).toMatchObject({
        count: true,
        filter: 'justin',
        _: ['justin'],
      });
    });
  });
});
