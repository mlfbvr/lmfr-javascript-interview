module.exports = function parseOptions(args) {
  const options = { _: [] };

  for (let i = 0; i < args.length; i++) {
    const arg = args[i];
    if (arg.startsWith('--')) {
      const [, key, value] = arg.match(/^--([^=]*)=?(.*)?/);
      options[key] = value || true;
    } else {
      options._ = options._.concat(arg);
    }
  }

  return options;
};
