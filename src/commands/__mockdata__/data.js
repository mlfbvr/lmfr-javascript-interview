module.exports = [
  {
    name: 'Canada',
    people: [
      {
        name: 'Justin Bieber',
        animals: [
          {
            name: 'John Wick',
          },
        ],
      },
    ],
  },
  {
    name: 'USA',
    people: [
      {
        name: 'Ariana Grande',
        animals: [
          {
            name: 'John Snow',
          },
        ],
      },
      {
        name: 'Mariah Carey',
        animals: [
          {
            name: 'John Doe',
          },
        ],
      },
    ],
  },
];
