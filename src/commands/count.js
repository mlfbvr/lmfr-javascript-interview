module.exports = {
  count,
};

// FIXME(mle): refactor implem as it is unreadable.

function count(tree) {
  return tree.map((country) => ({
    ...country,
    name: `${country.name} [${country.people.length}]`,
    people: country.people.map((people) => ({
      ...people,
      name: `${people.name} [${people.animals.length}]`,
    })),
  }));
}
