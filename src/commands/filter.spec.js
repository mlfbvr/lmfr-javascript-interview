const { filter } = require('./filter');
const mockdata = require('./__mockdata__/data');

describe('commands/filter', () => {
  describe('filter', () => {
    it('should return an empty array if no animal matches the needle', () => {
      expect(filter('needle', mockdata)).toMatchObject([]);
    });

    it('should return the same array if all animals matches the needle', () => {
      expect(filter('john', mockdata)).toMatchObject(mockdata);
    });

    it('should return only the canada if the needle is john wick', () => {
      expect(filter('john wick', mockdata)).toMatchObject([
        {
          name: 'Canada',
          people: [
            {
              name: 'Justin Bieber',
              animals: [
                {
                  name: 'John Wick',
                },
              ],
            },
          ],
        },
      ]);
    });

    it('should return only ariana grande if the needle is john snow', () => {
      expect(filter('john snow', mockdata)).toMatchObject([
        {
          name: 'USA',
          people: [
            {
              name: 'Ariana Grande',
              animals: [
                {
                  name: 'John Snow',
                },
              ],
            },
          ],
        },
      ]);
    });
  });
});
