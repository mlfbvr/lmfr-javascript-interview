const { count } = require('./count');
describe('commands/count', () => {
  describe('count', () => {
    it('should return an empty array if there is nothing to count', () => {
      expect(count([])).toMatchObject([]);
    });

    it('should count 0 if there are no people under a country', () => {
      expect(count([{ name: 'USA', people: [] }])).toMatchObject([
        { name: 'USA [0]', people: [] },
      ]);
    });

    it('should count the number of people under a country', () => {
      const [usa] = count([
        {
          name: 'USA',
          people: [
            { name: 'A', animals: [] },
            { name: 'B', animals: [] },
            { name: 'C', animals: [] },
          ],
        },
      ]);

      expect(usa.name).toBe('USA [3]');
    });

    it('should count 0 if people have no animals', () => {
      expect(
        count([
          { name: 'USA', people: [{ name: 'Ariana Grande', animals: [] }] },
        ])
      ).toMatchObject([
        {
          name: 'USA [1]',
          people: [{ name: 'Ariana Grande [0]', animals: [] }],
        },
      ]);
    });

    it('should count the number of animals under a person', () => {
      expect(
        count([
          {
            name: 'USA',
            people: [{ name: 'Ariana Grande', animals: [{}, {}] }],
          },
        ])
      ).toMatchObject([
        {
          name: 'USA [1]',
          people: [{ name: 'Ariana Grande [2]', animals: [{}, {}] }],
        },
      ]);
    });
  });
});
