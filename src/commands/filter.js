module.exports = {
  filter,
};

function filter(needle, tree) {
  const lowerNeedle = needle.toLowerCase();

  // FIXME(mle): refactor this as it is utterly unreadable.
  return tree
    .map((country) => ({
      ...country,
      people: country.people
        .map((people) => ({
          ...people,
          animals: people.animals.filter(({ name }) =>
            name.toLowerCase().includes(lowerNeedle)
          ),
        }))
        .filter(({ animals }) => animals.length > 0),
    }))
    .filter(({ people }) => people.length > 0);
}
