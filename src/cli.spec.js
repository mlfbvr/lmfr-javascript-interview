const { count } = require('./commands/count');
const { filter } = require('./commands/filter');

const main = require('./cli');

// jest.mock('./cli/parse_options');
jest.mock('./commands/count');
jest.mock('./commands/filter');

describe('cli', () => {
  describe('main', () => {
    it('should delegate to the filter command if it was called with --filter', () => {
      main(['--filter=ry']);

      expect(filter).toHaveBeenCalledTimes(1);
      expect(filter).toHaveBeenCalledWith('ry', expect.any(Array));
    });
    it('should delegate to the count command if it was called with --count', () => {
      main(['--count']);

      expect(count).toHaveBeenCalledTimes(1);
      expect(count).toHaveBeenCalledWith(expect.any(Array));
    });
    it('should throw an Error if called with anything else', () => {
      expect.assertions(1);
      try {
        main(['--anything_else']);
      } catch (e) {
        expect(e).toBeTruthy();
      }
    });
  });
});
