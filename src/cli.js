const parseOptions = require('./cli/parse_options');
const { data } = require('./data');
const { count } = require('./commands/count');
const { filter } = require('./commands/filter');

module.exports = function main(args) {
  const parsedArgs = parseOptions(args);

  if (parsedArgs.filter) {
    return filter(parsedArgs.filter, data);
  }

  if (parsedArgs.count) {
    return count(data);
  }

  // TODO(mle): better error handling
  throw Error('unknown args');
};
