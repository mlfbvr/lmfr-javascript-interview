const path = require('path');
const { spawn } = require('child_process');

/**
 *
 * @param {string} command - command ton run
 * @param {string} cwd - current working directory
 * @returns {Promise<{ code: number, data:string }>} - stderr if error, stdout otherwise
 */
const runCommand = (command, cwd = process.cwd()) => {
  const isCwdRelative = cwd.startsWith('.');

  if (isCwdRelative) {
    cwd = path.resolve(__dirname, cwd);
  }

  return new Promise((resolve, reject) => {
    let stdout = '';
    let stderr = '';
    const child = spawn(command, { cwd, shell: true });
    child.on('error', (error) => reject({ code: 1, data: error }));
    child.stdout.on('data', (data) => (stdout += data.toString()));
    child.stderr.on('data', (data) => (stderr += data.toString()));

    child.on('close', (code) => {
      if (stderr) {
        return reject({ code, data: stderr });
      }
      resolve({ code, data: stdout });
    });
  });
};

global.runCommand = runCommand;
