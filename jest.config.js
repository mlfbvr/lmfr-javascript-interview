module.exports = {
  setupFiles: ['./.jest/helpers/cli.js'],

  coverageDirectory: '<rootDir>/.reports/coverage',

  collectCoverageFrom: [
    'src/**/*.js',
    '!src/**/__mocks__/**/*.js',
    '!src/**/__mockdata__/**/*.js',
    '!src/data/**/*.js',
  ],
};
